#![no_std]
#![cfg_attr(feature = "bench", feature(test))]
#![cfg_attr(feature = "no_std", feature(alloc))]
#![cfg_attr(feature = "no_std", no_std)]
#![feature(nll)]

#[macro_use]
extern crate alloc;

pub use alloc::{borrow, rc};
pub use core::*;

pub mod collections {
    pub use alloc::collections::BTreeMap as HashMap;
    pub use alloc::collections::BTreeSet as HashSet;
}
pub mod prelude {
    pub use alloc::borrow::ToOwned;
    pub use alloc::boxed::Box;
    pub use alloc::string::String;
    pub use alloc::vec::Vec;
}


pub mod compile;
pub mod ctype;
pub mod immut_vec;
pub mod matcher;
pub mod tree;

pub use compile::PosixRegexBuilder;
pub use matcher::PosixRegex;
